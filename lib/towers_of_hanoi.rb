# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi

  attr_reader :towers

  def initialize
    @towers = [[3, 2, 1], [], []]
  end

  def play
    until won?
      puts render
      puts "Which tower do you want to move a disk from?"
      from_tower = gets.to_i
      puts "Which tower do you want to move a disk to?"
      to_tower = gets.to_i
      if valid_move?(from_tower, to_tower)
        move(from_tower, to_tower)
        display
      else
        display
          puts "Invalid move. Try again."
      end
    end
    puts "You win!" if won?
  end

  def render
    "\n" + "0: " + @towers[0].join + "\n" +
    "1: " + @towers[1].join + "\n" +
    "2: " + @towers[2].join + "\n"
  end

  def move(from_tower, to_tower)
    disk = @towers[from_tower].pop
    self.towers[to_tower].push(disk)
  end

  def valid_move?(from_tower, to_tower)
    return false if self.towers[from_tower].empty?
    if self.towers[to_tower].any?
      return false if self.towers[from_tower].last > self.towers[to_tower].last
    end
    true
  end

  def won?
    self.towers[2].size == 3 || self.towers[1].size == 3
  end

end

if __FILE__ == $PROGRAM_NAME
  TowersOfHanoi.new.play
end
